﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program7
{
    class Program
    {
        static void Main(string[] args)
        {
            var A = 0;
            var B = 1;

            Console.WriteLine("Currently, variable A = " + A + " and variable B = " + B);
            Console.Write("Enter 'swap' to swap the contents of the variables: ");

            var swapstatus = Console.ReadLine();

            if (swapstatus == "swap")
            {
                var status1 = A = B;
                var status2 = B = A -1;
                Console.WriteLine("Currently, variable A =  " + A + " and variable B =  " + B);
            }

            Console.ReadLine();
        }
    }
}
