﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program will print the perimeter and area of a circle based on it's radius");

            Console.Write("Please enter the radius: ");

            double radius = double.Parse(Console.ReadLine());
            double area = Math.PI * (Math.Pow(radius, 2));
            double perimeter = 2 * Math.PI * radius;

            Console.WriteLine("With a radius of " +radius+ "the perimeter of the circle is " +perimeter+ "and the area is " +area);

            Console.ReadLine();
        }
    }
}
