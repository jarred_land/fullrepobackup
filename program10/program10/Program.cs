﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Here is an array of random numbers");
            Console.WriteLine();

            int min = 0;
            int max = 20;

            int[] arrayint = new int[5];

            Random randNum = new Random();
            for (int i = 0; i < arrayint.Length; i++)
            {
                arrayint[i] = randNum.Next(min, max);
                Console.WriteLine(arrayint[i]);
            }

            Console.ReadLine();
        }
    }
}
