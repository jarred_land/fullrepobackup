﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program will print the multiplication table of your chosen digit up to 10");
            Console.WriteLine("Please enter your digit");

            var userinput = Console.ReadLine();

            int inputint = int.Parse(userinput.ToString());

            for(int count=1; count <= 10; count++)
            {
                Console.WriteLine(inputint + "*" + count + "=" + inputint*count);
            }

            Console.ReadLine();
        }
    }
}
