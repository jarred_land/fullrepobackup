﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("I will tell you if the number you have entered is positive or negative");
            Console.Write("Enter a number: ");

            int input = int.Parse(Console.ReadLine());

            if(input < 0)
            {
                Console.WriteLine("The number you have entered is negative");
            }
            else if(input > 0)
            {
                Console.WriteLine("The number you have entered is positive");
            }

            Console.ReadLine();
        }
    }
}
