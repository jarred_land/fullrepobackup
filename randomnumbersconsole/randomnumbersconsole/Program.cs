﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace randomnumbersconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press y to begin generating random numbers");
            var input = Console.ReadLine();

            while(input == "y")
            {
                Random rnd = new Random();
                int number = rnd.Next(1, 1000000000);
                Console.WriteLine(number);
            }
        }
    }
}
