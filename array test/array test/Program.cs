﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace array_test
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] intarray = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            string[] stringarray = new string[10] {"word1", "word2", "word3", "word4", "word5", "word6", "word7", "word8", "word9", "word10" };

            int intlength = intarray.Length;
            Console.WriteLine("The integer array contains " +intlength+ " items");

            int stringlength = stringarray.Length;
            Console.WriteLine();
            Console.WriteLine("The string array contains " +stringlength+ " items");
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("The arrays will now be copied to new arrays");

            int[] intarray2 = new int[10];
            string[] stringarray2 = new string[10];

            intarray.CopyTo(intarray2, 0);
            stringarray.CopyTo(stringarray2, 0);

            Console.WriteLine("Int array 2 now contains " +intarray2.Length+ " items from int array 1");
            Console.WriteLine("String array 2 now contains " + stringarray2.Length + " items from string array 1");
            Console.WriteLine();

            Console.WriteLine("These are the values copied from the integer array");
            for (int i = 0; i < intarray2.Length; i++)
            {
                Console.WriteLine(intarray2[i]);
            }
            Console.WriteLine();
            Console.WriteLine("These are the values copied from the integer array");
            for (int i = 0; i < stringarray2.Length; i++)
            {
                Console.WriteLine(stringarray2[i]);
            }

            Console.WriteLine("--------------------------------------------------------------");

            Type inttype = intarray.GetType().GetElementType();
            Type stringtype = stringarray.GetType().GetElementType();
            stringarray.GetValue(4);
            Console.WriteLine("the value at index 5 is {0},  ", stringarray.GetValue(5));

            Console.WriteLine("The integer array is of type: " +inttype+ " and the string array is of type: " +stringtype);
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("The item in index #5 of the int array is: "+intarray[5]);
            Console.WriteLine("The item in index #5 of the string array is: " + stringarray[5]);
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine();

            int searchintid = 9;
            int intindexid = Array.IndexOf(intarray, searchintid);
            Console.WriteLine("The searched number {0} is at index {1} ", searchintid, intindexid);
            Console.WriteLine();
            string searchname = "word3";
            int indexname = Array.IndexOf(stringarray, searchname);
            Console.WriteLine("The searched letter {0} is at index {1} ", searchname, indexname);

            Console.WriteLine("--------------------------------------------------------------");

            Console.WriteLine();

            Array.Reverse(intarray2);
            Console.WriteLine("When reversed, the array reads: ");

            foreach (int i in intarray2)
            {
                Console.WriteLine(i + " ");
            }
            Console.WriteLine("--------------------------------------------------------------");

            int[] newintarray = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            newintarray.SetValue(11, 5);
            Console.WriteLine("New value at location five: ");

            foreach (int i in newintarray)
            {
                Console.WriteLine(i + " ");
            }

            Console.WriteLine("--------------------------------------------------------------");

            Array.Sort(intarray);
            Console.Write("Sorted int Array in ascending order: ");

            foreach (int i in intarray)
            {
                Console.Write(i + " ");
            }

            Console.WriteLine();

            Array.Sort(stringarray);
            Console.Write("Sorted string Array in ascending order: ");

            foreach (string i in stringarray)
            {
                Console.Write(i + " ");
            }

            Console.WriteLine();

            Array.Sort(intarray);
            Console.Write("Sorted int Array in descending order: ");

            foreach (int i in intarray)
            {
                Console.Write(i + " ");
            }

            Console.WriteLine();

            Array.Reverse(stringarray);
            Console.Write("Sorted string Array in descending order: ");

            foreach (string i in stringarray)
            {
                Console.Write(i + " ");
            }



            Console.ReadLine();

        }
    }
}
